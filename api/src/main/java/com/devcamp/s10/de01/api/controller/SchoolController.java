package com.devcamp.s10.de01.api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.de01.api.model.School;
import com.devcamp.s10.de01.api.service.SchoolService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class SchoolController {
    @Autowired
    private SchoolService schoolService;

    @GetMapping("/schools")
    public ArrayList<School> getAllSchools() {
        ArrayList<School> allSchools = schoolService.getSchoolList();
        return allSchools;
    }

    @GetMapping("schools-info")
    public School getSchools(@RequestParam(value = "schoolId", required = true) int schoolId) {
        ArrayList<School> allSchools = schoolService.getSchoolList();
        School requestSchool = new School();
        for (School school: allSchools){
            if (school.getId()== schoolId){
                requestSchool = school;
            }
        }
        return  requestSchool;
    }

    @GetMapping("schools-student")
    public ArrayList<School> getSchoolNoStudent(@RequestParam(name  = "noNumber", required = true) int noNumber) {
        ArrayList<School> allSchools = schoolService.getSchoolList();
        ArrayList<School> newSchoolList = new ArrayList<>();
        for (School school: allSchools){
            if (school.getTotalStudent() > noNumber){
                newSchoolList.add(school);
            }
        }
        return  newSchoolList;
    }
}
