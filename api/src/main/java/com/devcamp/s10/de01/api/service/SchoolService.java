package com.devcamp.s10.de01.api.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s10.de01.api.model.School;

@Service
public class SchoolService {
    @Autowired
    private ClassroomService classroomService;

    private School primary = new School(1, "Primary", "HCM");
    private School secondary = new School(2, "Secondary", "HN");
    private School highschool = new School(3, "Highschool", "DN");

    public ArrayList<School> getSchoolList() {
        primary.setClassrooms(classroomService.getPrimaryClassroom());
        secondary.setClassrooms(classroomService.getSecondaryClassroom());
        highschool.setClassrooms(classroomService.getHighSchoolClassroom());

        ArrayList<School> schoolList = new ArrayList<>();
        schoolList.add(primary);
        schoolList.add(secondary);
        schoolList.add(highschool);
        return schoolList;
    }
}
