package com.devcamp.s10.de01.api.service;

import java.util.ArrayList;
import org.springframework.stereotype.Service;

import com.devcamp.s10.de01.api.model.Classroom;

@Service
public class ClassroomService {
    //tạo 9 classroom cho 3 school
  private  Classroom classroom1 = new Classroom(1, "Grade1", 40);
  private  Classroom classroom2 = new Classroom(2, "Grade2", 50);
  private  Classroom classroom3 = new Classroom(3, "Grade3", 35);

  private  Classroom classroom6 = new Classroom(6, "Grade6", 30);
  private  Classroom classroom7 = new Classroom(7, "Grade7", 45);
  private  Classroom classroom8 = new Classroom(8, "Grade8", 35);

  private  Classroom classroom10 = new Classroom(10, "Grade10", 40);
  private  Classroom classroom11 = new Classroom(11, "Grade11", 30);
  private  Classroom classroom12 = new Classroom(12, "Grade12", 45);

    //get danh sách tất cả các classroom
    public ArrayList<Classroom> getClassroomList() {
        ArrayList<Classroom> classroomList = new ArrayList<Classroom>();
        classroomList.add(classroom1);
        classroomList.add(classroom2);
        classroomList.add(classroom3);
        classroomList.add(classroom6);
        classroomList.add(classroom7);
        classroomList.add(classroom8);
        classroomList.add(classroom10);
        classroomList.add(classroom11);
        classroomList.add(classroom12);

        return classroomList;
    }
    //get danh sách tất cả các classroom primary school
    public ArrayList<Classroom> getPrimaryClassroom() {
        ArrayList<Classroom> primaryList = new ArrayList<Classroom>();
        primaryList.add(classroom1);
        primaryList.add(classroom2);
        primaryList.add(classroom3);

        return primaryList;
    }
    //get danh sách tất cả các classroom secondary school
    public ArrayList<Classroom> getSecondaryClassroom() {
        ArrayList<Classroom> secondaryList = new ArrayList<Classroom>();
        secondaryList.add(classroom6);
        secondaryList.add(classroom7);
        secondaryList.add(classroom8);

        return secondaryList;
    }
    //get danh sách tất cả các classroom high school
    public ArrayList<Classroom> getHighSchoolClassroom() {
        ArrayList<Classroom> highSchoolList = new ArrayList<Classroom>();
        highSchoolList.add(classroom10);
        highSchoolList.add(classroom11);
        highSchoolList.add(classroom12);

        return highSchoolList;
    }
}
