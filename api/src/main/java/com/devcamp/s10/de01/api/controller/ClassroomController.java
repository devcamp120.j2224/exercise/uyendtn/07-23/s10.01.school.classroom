package com.devcamp.s10.de01.api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.de01.api.model.Classroom;
import com.devcamp.s10.de01.api.service.ClassroomService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ClassroomController {
    @Autowired
     private ClassroomService classroomService;
     

     @GetMapping("/classrooms")
     public ArrayList<Classroom> getAllClassrooms() {
        ArrayList<Classroom> allClassrooms = classroomService.getClassroomList();
        return allClassrooms;
     }

     @GetMapping("/classrooms-info")
     public ArrayList<Classroom> getClassroomNoStudent(@RequestParam(name = "noNumber", required = true) int noNumber) {
        ArrayList<Classroom> allClassrooms = classroomService.getClassroomList();
        ArrayList<Classroom> newClassroomList = new ArrayList<Classroom>();
        for (Classroom classroom:allClassrooms){
           if (classroom.getNoStudent() > noNumber){
            newClassroomList.add(classroom);
           }
        }
        return newClassroomList;
     }
}
